import React, { Component } from 'react';
import { throttle } from 'underscore';

import { fetchUsers } from '../api';
import { SearchField } from '../components';
import { HomeContainer } from '../theme';

class Home extends Component {
  state = { isFocused: false, isTyping: false, results: [], searchText: '' };

  handleFocus = () => {
    this.setState({ isFocused: true });
  };

  handleBlur = e => {
    if (e && e.relatedTarget) {
      e.relatedTarget.click();
    }
    this.setState({ isFocused: false });
  };

  handleSearchChange = event => {
    this.setState({
      isTyping: true,
      isFetching: false,
      searchText: event.target.value,
      isError: false
    });
    this.handleSearchSubmit(event.target.value);
  };

  handleSearchSubmit = throttle(async term => {
    this.setState({ isFetching: true });
    const users = await fetchUsers(term);

    this.setState({ isTyping: false, results: users, isFetching: false });
  }, 400);

  componentWillUnmount = () => {
    this.handleSearchSubmit.cancel();
  };

  render() {
    return (
      <HomeContainer>
        <SearchField
          onSearchFocus={this.handleFocus}
          onSearchChange={this.handleSearchChange}
          onSearchBlur={this.handleBlur}
          onSearchInput={this.handleSearchInput}
          searchText={this.state.searchText}
          users={this.state.results}
          isTyping={this.state.isTyping}
          isFocused={this.state.isFocused}
        />
      </HomeContainer>
    );
  }
}

export default Home;
