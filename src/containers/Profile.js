import React, { Component } from 'react';

import { ProfileContainer, GridContainer } from '../theme';
import { User, Details } from '../components';
import {
  fetchUser,
  fetchWork,
  fetchProjects,
  fetchFollowers,
  fetchFollowing
} from '../api';

class Profile extends Component {
  state = {
    user: null,
    projects: [],
    works: [],
    followers: [],
    following: [],
    isFetching: false
  };

  componentDidMount = async () => {
    this.setState({ isFetching: true });
    const { id } = this.props.match.params;

    try {
      const works = await fetchWork(id);
      const user = await fetchUser(id);
      const projects = await fetchProjects(id);
      const followers = await fetchFollowers(id);
      const following = await fetchFollowing(id);
      this.setState({
        user,
        projects,
        works,
        following,
        followers,
        isFetching: false
      });
    } catch (err) {
      console.log('error with an api request', err);
    }
  };

  render() {
    const {
      isFetching,
      user,
      projects,
      followers,
      following,
      works
    } = this.state;

    return (
      <ProfileContainer>
        {isFetching ? (
          <div>fetching data...</div>
        ) : (
          <GridContainer>
            {user && (
              <User user={user} projectCount={projects.length} works={works} />
            )}
            <Details
              works={works}
              projects={projects}
              followers={followers}
              following={following}
              match={this.props.match}
            />
          </GridContainer>
        )}
      </ProfileContainer>
    );
  }
}

export default Profile;
