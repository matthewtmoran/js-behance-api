import React from 'react';
import { shallow } from 'enzyme';
import Home from './Home';
import '../setupTests';

describe('Home tests', () => {
  it('renders Home component ', () => {
    const wrapper = shallow(<Home />);
    expect(wrapper).toMatchSnapshot();
  });
});
