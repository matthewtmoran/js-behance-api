import React from 'react';
import { shallow } from 'enzyme';
import Profile from './Profile';
import '../setupTests';

describe('Profile tests', () => {
  it('renders Profile component ', () => {
    const wrapper = shallow(<Profile />);
    wrapper.state({ isFetching: false });
    expect(wrapper).toMatchSnapshot();
  });
});
