import React from 'react';
import PropTypes from 'prop-types';

import {
  DropdownContainer,
  StyledLink,
  Ul,
  H3,
  Li,
  Img,
  Username,
  Occupation
} from '../theme';

const Dropdown = ({ users }) => {
  return (
    <DropdownContainer>
      <Ul>
        {users.length ? (
          users.map(user => {
            return (
              <Li key={user.id}>
                <StyledLink to={{ pathname: `/profile/${user.id}/projects` }}>
                  <Img src={user.images[50]} alt="thumbnail" />
                  <Username>{user.display_name}</Username>
                  <Occupation>{user.occupation}</Occupation>
                </StyledLink>
              </Li>
            );
          })
        ) : (
          <Li>
            <H3>No results</H3>
          </Li>
        )}
      </Ul>
    </DropdownContainer>
  );
};

Dropdown.propTypes = {
  users: PropTypes.array
};

export default Dropdown;
