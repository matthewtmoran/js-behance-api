import React from 'react';
import { shallow } from 'enzyme';
import SearchField from './SearchField';
import '../setupTests';

describe('SearchField tests', () => {
  it('renders SearchField component ', () => {
    const searchText = 'this is text';
    const users = [];
    const isTyping = false;
    const onSearchChange = jest.fn();
    const onSearchFocus = jest.fn();
    const onSearchBlur = jest.fn();

    const wrapper = shallow(
      <SearchField
        searchText={searchText}
        users={users}
        isTyping={isTyping}
        onSearchBlur={onSearchBlur}
        onSearchFocus={onSearchFocus}
        onSearchChange={onSearchChange}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
