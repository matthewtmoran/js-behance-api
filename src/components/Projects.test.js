import React from 'react';
import { shallow } from 'enzyme';
import Projects from './Projects';
import '../setupTests';

describe('Projects tests', () => {
  it('renders Projects component ', () => {
    const projects = [
      {
        id: 123,
        url: 'http://behance.com',
        covers: {
          '250': 'http://behance.com'
        },
        fields: ['this ', ' is', 'field'],
        name: 'this is name',
        stats: { views: 1234, appreciations: 113, comments: 12 }
      },
      {
        id: 1234,
        url: 'http://behance.com',
        covers: {
          '250': 'http://behance.com'
        },
        fields: ['this ', ' is', 'field'],
        name: 'this is name',
        stats: { views: 1234, appreciations: 113, comments: 12 }
      }
    ];
    const wrapper = shallow(<Projects project={projects} />);
    expect(wrapper).toMatchSnapshot();
  });
});
