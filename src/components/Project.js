import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  ProjectContainer,
  ImgContainer,
  ProjectImg,
  Info,
  Field,
  Stats,
  ProjectName,
  Stat,
  ProjectLink
} from '../theme';

const Project = ({ project }) => {
  return (
    <ProjectContainer>
      <ProjectLink href={project.url} target="_blank">
        <ImgContainer>
          <ProjectImg src={project.covers[230]} alt="" />
          <Info className="overlay">
            {project.fields.map(field => (
              <Field key={field}>{field}</Field>
            ))}
          </Info>
        </ImgContainer>
        <ProjectName>{project.name}</ProjectName>
        <Stats>
          <Stat>
            <FontAwesomeIcon icon={['fas', 'eye']} />
            {project.stats.views}
          </Stat>
          <Stat>
            <FontAwesomeIcon icon={['fas', 'thumbs-up']} />
            {project.stats.appreciations}
          </Stat>
          <Stat>
            <FontAwesomeIcon icon={['fas', 'comments']} />
            {project.stats.comments}
          </Stat>
        </Stats>
      </ProjectLink>
    </ProjectContainer>
  );
};

Project.propTypes = {
  project: PropTypes.object
};

export default Project;
