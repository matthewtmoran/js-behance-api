import React from 'react';
import { shallow } from 'enzyme';
import Follows from './Follows';
import '../setupTests';

describe('Follows tests', () => {
  it('renders Follows component ', () => {
    const follows = [
      {
        id: 1234,
        url: 'http://behance.com',
        display_name: 'matthewmoran',
        images: { '115': 'http://behance.com' },
        occupation: 'occupation',
        fields: ['this', 'is', 'field']
      }
    ];

    const wrapper = shallow(<Follows follows={follows} />);
    expect(wrapper).toMatchSnapshot();
  });
});
