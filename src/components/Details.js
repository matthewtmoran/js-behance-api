import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import { Projects, Follows } from './index';
import { DetailsContainer, Tabs, TabLink } from '../theme';

const Details = ({ following, followers, projects, match }) => {
  return (
    <DetailsContainer>
      <Tabs>
        <TabLink activeClassName="active" to={`${match.url}/projects`}>
          Projects
        </TabLink>
        <TabLink activeClassName="active" to={`${match.url}/followers`}>
          Followers
        </TabLink>
        <TabLink activeClassName="active" to={`${match.url}/following`}>
          Following
        </TabLink>
      </Tabs>

      <Route
        path={`${match.path}/followers`}
        render={props => <Follows {...props} follows={followers} />}
      />
      <Route
        path={`${match.path}/following`}
        render={props => <Follows {...props} follows={following} />}
      />
      <Route
        path={`${match.path}/projects`}
        render={props => <Projects projects={projects} {...props} />}
      />
    </DetailsContainer>
  );
};

Details.propTypes = {
  following: PropTypes.array,
  followers: PropTypes.array,
  projects: PropTypes.array,
  match: PropTypes.object
};

export default Details;
