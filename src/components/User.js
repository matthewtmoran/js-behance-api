import React from 'react';
import PropTypes from 'prop-types';

import {
  UserContainer,
  ProfileImage,
  Details,
  Item,
  UserOccupation,
  Company,
  FancyLink,
  Group,
  About,
  Social,
  DisplayName,
  Emph,
  SocialIcon,
  LinkIcon,
  Position
} from '../theme';
import { intToString } from '../Utils';

const User = ({ user, projectCount, works }) => {
  return (
    <UserContainer>
      <ProfileImage img={user.images[276]} />
      <Details>
        <Group>
          <DisplayName>{user.display_name}</DisplayName>
          <UserOccupation>{user.occupation}</UserOccupation>
        </Group>
        <Group>
          {user.company && <Company>{user.company}</Company>}
          {user.website && (
            <Item>
              <FancyLink href={user.website} target="_blank">
                <LinkIcon icon={['fas', 'link']} />
                {user.website.replace(/(^https?:\/\/www.)|(\/$)/g, '')}
              </FancyLink>
            </Item>
          )}
        </Group>
        <Group>
          <About>{user.sections.About}</About>
        </Group>
        <Group>
          <Item>
            <Emph>{intToString(user.stats.views)}</Emph> Views
          </Item>
          <Item>
            <Emph>{intToString(user.stats.followers)}</Emph> Followers
          </Item>
          <Item>
            <Emph>{intToString(user.stats.following)}</Emph> Following
          </Item>
          <Item>
            <Emph>{intToString(user.stats.appreciations)}</Emph> Appreciations
          </Item>
          <Item>
            <Emph>{intToString(projectCount)}</Emph> Projects
          </Item>
        </Group>

        {works.length > 1 && (
          <Group>
            <h4>Work Experience</h4>
            {works.map(work => {
              return (
                <Group key={work.organization}>
                  <Position>{work.position}</Position>
                  <UserOccupation>{work.organization}</UserOccupation>
                </Group>
              );
            })}
          </Group>
        )}

        <Social>
          <FancyLink href={`mailto:${user.sections.Contact}`}>
            <SocialIcon icon={['fas', 'envelope']} />
          </FancyLink>

          {user.social_links.map(social => {
            return (
              <FancyLink
                key={social.social_id}
                href={social.url}
                target="_blank"
              >
                <SocialIcon
                  icon={['fab', `${social.service_name.toLowerCase()}`]}
                />
              </FancyLink>
            );
          })}
        </Social>
      </Details>
    </UserContainer>
  );
};

User.propTypes = {
  user: PropTypes.object,
  projectCount: PropTypes.number,
  works: PropTypes.array
};

export default User;
