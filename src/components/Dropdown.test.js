import React from 'react';
import { shallow } from 'enzyme';
import Dropdown from './Dropdown';
import '../setupTests';

describe('Dropdown tests', () => {
  const users = [
    {
      id: 1234,
      images: {
        '50': 'http://aurl.com/'
      },
      display_name: 'matthewmoran',
      occupation: 'developer'
    },
    {
      id: 234,
      images: {
        '50': 'http://aurl.com/'
      },
      display_name: 'user',
      occupation: 'developer'
    }
  ];
  it('renders Dropdown component ', () => {
    const wrapper = shallow(<Dropdown users={users} />);
    expect(wrapper).toMatchSnapshot();
  });
});
