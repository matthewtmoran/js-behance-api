import React from 'react';
import { shallow } from 'enzyme';
import User from './User';
import '../setupTests';

describe('User tests', () => {
  it('renders User component ', () => {
    const user = {
      display_name: 'MatthewMoran',
      occupation: 'Developer',
      company: 'mmoranmedia',
      website: 'mmoranmedia.com',
      images: { '276': 'aurl' },
      sections: [{ About: 'lorem ipsum ' }, { Contact: 'test@gmail.com' }],
      stats: {
        view: 1,
        followers: 1000000,
        following: 1000,
        appreciations: 10000,
        appreciations: 1000000
      },
      social_links: [
        {
          social_id: 2,
          url: 'http://facebook.com/gletscherlinz',
          service_name: 'Facebook'
        },
        {
          social_id: 12,
          url: 'http://instagram.com/gletscher_creative',
          service_name: 'Instagram'
        }
      ]
    };
    const projectCount = 12;
    const works = [
      { organization: 'mmoranmedia', position: 'Dev' },
      { organization: 'other', position: 'dev' }
    ];
    const wrapper = shallow(
      <User user={user} projectCount={projectCount} works={works} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
