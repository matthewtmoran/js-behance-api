import React from 'react';
import { shallow } from 'enzyme';
import Details from './Details';
import '../setupTests';

describe('Details tests', () => {
  it('renders Details component ', () => {
    const following = [
      {
        id: 1234,
        display_name: 'test'
      }
    ];
    const followers = [
      {
        id: 1234,
        display_name: 'test'
      }
    ];
    const projects = [
      {
        id: 1234,
        name: 'my project'
      }
    ];
    const match = { url: 'http://baseUrl/', path: 'http://baseUrl/' };
    const wrapper = shallow(
      <Details
        following={following}
        followers={followers}
        projects={projects}
        match={match}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
