import React from 'react';
import PropTypes from 'prop-types';

import { Project } from './index';
import { ProjectsGrid } from '../theme';

const Projects = ({ projects }) => {
  return (
    <ProjectsGrid>
      {projects &&
        projects.map(project => <Project key={project.id} project={project} />)}
    </ProjectsGrid>
  );
};

Projects.propTypes = {
  projects: PropTypes.array
};

export default Projects;
