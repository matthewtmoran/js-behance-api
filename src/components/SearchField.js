import React from 'react';
import PropTypes from 'prop-types';

import {
  SearchContainer,
  InputWrapper,
  StyledInput,
  StyledSearch,
  StyledSpinner
} from '../theme';

import { Dropdown } from '../components';

const SearchField = ({
  searchText,
  users,
  onSearchChange,
  onSearchFocus,
  onSearchBlur,
  isTyping,
  isFocused
}) => {
  return (
    <SearchContainer>
      <InputWrapper>
        <StyledSearch icon={['fas', 'search']} size="2x" />

        <StyledInput
          onBlur={onSearchBlur}
          onFocus={onSearchFocus}
          onChange={onSearchChange}
          placeholder="Search Users"
          value={searchText}
          type="text"
        />

        {isTyping && <StyledSpinner icon={['fas', 'spinner']} spin size="2x" />}
      </InputWrapper>
      {isFocused &&
        searchText.length > 0 && (
          <Dropdown isFocused={isFocused} users={users} />
        )}
    </SearchContainer>
  );
};

SearchField.propTypes = {
  searchText: PropTypes.string,
  isFocused: PropTypes.bool,
  users: PropTypes.array,
  onSearchChange: PropTypes.func,
  onSearchFocus: PropTypes.func,
  onSearchBlur: PropTypes.func
};

export default SearchField;
