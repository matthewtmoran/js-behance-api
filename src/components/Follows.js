import React from 'react';
import PropTypes from 'prop-types';

import {
  FollowsContainer,
  Row,
  FollowImg,
  FollowName,
  FollowSecondary
} from '../theme';

const Follows = ({ follows }) => {
  return (
    <FollowsContainer>
      {follows.map(follow => (
        <Row key={follow.id} href={follow.url} target="_blank">
          <div className="overlay" />
          <FollowImg src={follow.images[115]} alt="" />
          <FollowName>
            {follow.display_name} || {follow.username}
          </FollowName>
          <FollowSecondary>{follow.occupation}</FollowSecondary>
          <FollowSecondary>{follow.fields.join(' / ')}</FollowSecondary>
        </Row>
      ))}
    </FollowsContainer>
  );
};

Follows.propTypes = {
  follows: PropTypes.array
};

export default Follows;
