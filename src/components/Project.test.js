import React from 'react';
import { shallow } from 'enzyme';
import Project from './Project';
import '../setupTests';

describe('Project tests', () => {
  it('renders Project component ', () => {
    const project = {
      url: 'http://behance.com',
      covers: {
        '250': 'http://behance.com'
      },
      fields: ['this ', ' is', 'field'],
      name: 'this is name',
      stats: { views: 1234, appreciations: 113, comments: 12 }
    };
    const wrapper = shallow(<Project project={project} />);
    expect(wrapper).toMatchSnapshot();
  });
});
