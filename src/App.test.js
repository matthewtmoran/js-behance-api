import React from 'react';
// import ReactDOM from 'react-dom';
import App from './App';
import renderer from 'react-test-renderer';

describe('App Component', () => {
  it('matches the snapshot', () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
