import axios from 'axios';
import { USER_API, USERS_API } from './constants';

//fetches all users
export const fetchUsers = async term => {
  const url = `${USERS_API}${term}`;
  const res = await axios.get(url);
  const data = await res.data;
  return data.users;
};

//fetches single user data
export const fetchUser = async userId => {
  const url = `${USER_API}${userId}`;
  const res = await axios.get(url);
  const data = await res.data;
  return data.user;
};

//fetches user projects
export const fetchProjects = async userId => {
  const url = `${USER_API}${userId}/projects`;
  const res = await axios.get(url);
  const data = await res.data;
  return data.projects;
};

//fetches users work experience
export const fetchWork = async userId => {
  try {
    const url = `${USER_API}${userId}/work`;
    const res = await axios.get(url);
    const data = await res.data;
    return data.work_experience;
  } catch (err) {
    return err;
  }
};

//fetches users followers
export const fetchFollowers = async userId => {
  const url = `${USER_API}${userId}/followers`;
  const res = await axios.get(url);
  const data = await res.data;
  return data.followers;
};

//fetches users following
export const fetchFollowing = async userId => {
  const url = `${USER_API}${userId}/following`;
  const res = await axios.get(url);
  const data = await res.data;
  return data.following;
};
