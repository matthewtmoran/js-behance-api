import { intToString } from './index';

describe('intToString utility fn', () => {
  it('converts number to string', () => {
    expect(intToString(1)).toEqual('1');
  });

  it('abbreviates 1000 to "k"', () => {
    expect(intToString(1000)).toEqual('1k');
  });

  it('abbreviates 100000 with "m"', () => {
    expect(intToString(1000000)).toMatch('1m');
  });
});
