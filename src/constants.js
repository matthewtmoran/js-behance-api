const API_KEY = 'JS7Sn6wBTD62xb0gS9Amj1Zf06C4esI5';
const BASE_URL = 'http://api.behance.net/v2/';
const USERS_URL = `${BASE_URL}users/?q=`;
const USER_URL = `${BASE_URL}users/`;

const BASE_API_URL = `http://localhost:5000`;
const USERS_API = `${BASE_API_URL}/users/?term=`;
const USER_API = `${BASE_API_URL}/user/`;

module.exports = {
  API_KEY,

  BASE_URL,
  USERS_URL,
  USER_URL,

  BASE_API_URL,
  USERS_API,
  USER_API
};
