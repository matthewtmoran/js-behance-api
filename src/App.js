import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { Home, Profile } from './containers';
import './App.css';
import {
  faEnvelope,
  faLink,
  faEye,
  faThumbsUp,
  faComments,
  faSpinner,
  faSearch
} from '@fortawesome/free-solid-svg-icons';
import {
  fab,
  faTwitter,
  faInstagram,
  faTumblr,
  faLinkedin,
  faPinterest,
  faDribbble,
  faFacebook
} from '@fortawesome/free-brands-svg-icons';

library.add(
  fab,
  faTwitter,
  faInstagram,
  faTumblr,
  faLinkedin,
  faPinterest,
  faDribbble,
  faFacebook,
  faEnvelope,
  faLink,
  faEye,
  faThumbsUp,
  faComments,
  faSpinner,
  faSearch
);

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/profile/:id" component={Profile} />
        </Switch>
      </Router>
    );
  }
}

export default App;
