import styled from 'styled-components';
import { NavLink, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//Profile
export const ProfileContainer = styled.div`
  position: relative;
  height: 100%;
  background: #193549;
  overflow: auto;
`;

export const GridContainer = styled.div`
  display: flex;
  flex-direction: row;

  position: absolute;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  width: 90%;
  height: 90%;

  @media (max-width: 700px) {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    justify-content: flex-start;
    align-items: stretch;
  }
`;

//Home
export const HomeContainer = styled.div`
  position: relative;
  height: 100%;
  background: #193549;
  overflow: auto;
`;

//Details
export const DetailsContainer = styled.div`
  flex: 5;
  margin: 0 12px;

  height: 100%;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
  @media (max-width: 700px) {
    margin-top: 30px;
    margin-right: 0;
    margin-left: 0;
    flex: none;
    width: 100%;
  }
`;

export const Tabs = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  height: 60px;
  margin-bottom: 12px;
`;

export const TabLink = styled(NavLink)`
  text-align: center;
  padding: 12px;
  margin: 0 50px;
  flex: 1;
  text-decoration: none;
  border-bottom: 2px solid transparent;
  position: relative;

  &:after {
    -webkit-transform: scaleX(0);
    -ms-transform: scaleX(0);
    transform: scaleX(0);

    content: ' ';
    position: absolute;
    bottom: -3px;
    left: 0;
    width: 100%;
    height: 2px;
    background: #334858;

    content: ' ';
    position: absolute;
    bottom: -3px;
    left: 0;
    width: 100%;
    height: 2px;
    background: #ffc600;
    transition: 0.2s cubic-bezier(0.455, 0.03, 0.515, 0.955);
    -webkit-transition: 0.2s cubic-bezier(0.455, 0.03, 0.515, 0.955);
    -moz-transition: 0.2s cubic-bezier(0.455, 0.03, 0.515, 0.955);
    -webkit-transform: scaleX(0);
    -ms-transform: scaleX(0);
    transform: scaleX(0);
  }

  &.active {
    background: #234e6d;
  }

  &.active,
  &:hover {
    &:after {
      -webkit-transform: scaleX(0.85);
      -ms-transform: scaleX(0.85);
      transform: scaleX(0.85);

      -webkit-transform: scaleX(1);
      -ms-transform: scaleX(1);
      transform: scaleX(1);
    }
  }

  @media (max-width: 700px) {
    margin: 0;
  }
`;

//Dropdown
export const DropdownContainer = styled.div`
  color: #fff;
  border-radius: 3px;
  margin: auto;
  width: 48vw;
  position: relative;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  z-index: 3;
  max-height: 66vh;
  overflow-y: auto;
  overflow-x: hidden;
`;

export const StyledLink = styled(Link)`
  color: #fff;
  font-weight: bold;
  text-decoration: none;
  flex-direction: row;

  width: 100%;
  height: 100%;
  padding: 8px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const Ul = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
`;

export const H3 = styled.h3`
  margin: 0;
`;

export const Li = styled.li`
  &:hover {
    cursor: pointer;
    transition: 0.3s;
    background: #ff720066;
  }
`;

export const Img = styled.img`
  border-radius: 50%;
  padding: 0 16px;
`;

export const Username = styled.span`
  font-size: 20px;
  padding: 0 8px;
`;

export const Occupation = styled.span`
  font-size: 16px;
  font-style: italic;
  color: rgba(255, 255, 255, 0.7);
  padding: 0 8px;
`;

//Follows

export const FollowsContainer = styled.div`
  overflow-x: hidden;
  display: flex;
  flex-direction: row;
  flex: auto;
  flex-wrap: wrap;
  justify-content: space-between;
  height: 100%;
`;

export const Row = styled.a`
  height: 110px;
  text-decoration: none;
  padding: 12px;
  width: 100%;
  display: flex;
  justify-content: start;
  align-items: center;
  position: relative;

  .overlay {
    background: #ffc600;
    opacity: 0;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
    transition: 0.2s;
  }

  &:hover {
    .overlay {
      opacity: 0.3;
    }
  }
`;

export const FollowItem = styled.span`
  margin: 0 28px;
`;

export const FollowImg = styled.img`
  border-radius: 50%;
  height: 100%;
  margin: 0 24px;
`;

export const FollowName = styled(FollowItem)`
  font-size: 22px;
`;

export const FollowSecondary = styled(FollowItem)`
  color: rgba(255, 255, 255, 0.6);
  font-size: 18px;
`;

//Project

export const ProjectContainer = styled.div`
  overflow: hidden;
  display: flex;
  width: 23%;
  flex-direction: column;
  margin-bottom: 24px;
  margin-right: 8px;
  margin-left: 8px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;

export const ImgContainer = styled.div`
  position: relative;
  overflow: hidden;
`;

export const ProjectImg = styled.img`
  display: block;
  width: 100%;
  max-width: 100%;
  height: auto;
  transition: 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955);
`;

export const Info = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  background: rgba(0, 0, 0, 0.6);
  transition: 0.2s;
  opacity: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Field = styled.div`
  margin: 8px;
  font-size: 20px;
  letter-spacing: 3px;
`;

export const Stats = styled.div`
  width: 100%;
  z-index: 2;
  padding: 12;
  font-size: 14px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  color: rgba(255, 255, 255, 0.6);
`;

export const ProjectName = styled.div`
  padding: 12px;
  text-align: center;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  transition: 0.2s;
`;

export const Stat = styled.span`
  padding: 8px;
  margin: 8px 12px;
  color: rgba(255, 255, 255, 0.6);
`;

export const ProjectLink = styled.a`
  height: 100%;
  width: 100%;
  text-decoration: none;
  :hover {
    img {
      transform: scale(1.05, 1.05);
    }
    .overlay {
      opacity: 1;
    }
  }
`;

//Projects

export const ProjectsGrid = styled.div`
  overflow-x: hidden;
  display: flex;
  flex-direction: row;
  flex: auto;
  flex-wrap: wrap;
  justify-content: space-between;
  height: 100%;
`;

// SearchField

export const SearchContainer = styled.div`
  position: absolute;
  height: 50px;
  width: 50vw;
  margin: auto;
  left: 0;
  right: 0;
  top: 16vh;
  text-align: center;
`;

export const InputWrapper = styled.div`
  min-width: 125px;
  width: 100%;
  position: relative;
`;

export const StyledInput = styled.input`
  border: 2px solid #234e6d;
  border-radius: 15%;
  color: rgba(255, 255, 255, 0.9);
  font-size: 30px;
  font-weight: bold;
  background-color: #1f4662;
  border-radius: 70px;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  display: block;
  height: 60px;
  line-height: 30px;
  padding: 8px 32px 8px 60px;
  transition: all 0.2s ease-in-out;
  width: 100%;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);

  &::placeholder {
    color: #27577a;
  }

  &:input {
    outline: none;
  }
`;

export const StyledSearch = styled(FontAwesomeIcon)`
  position: absolute;
  left: 16px;
  top: 0;
  bottom: 0;
  margin: auto;
  color: rgba(255, 255, 255, 0.9);
`;

export const StyledSpinner = styled(FontAwesomeIcon)`
  position: absolute;
  right: 16px;
  top: 0;
  bottom: 0;
  margin: auto;
  color: rgba(255, 255, 255, 0.9);
`;

//User
export const UserContainer = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  margin: 0 12px;
  flex: 1;

  height: 100%;
  background: #122738;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);

  @media (max-width: 700px) {
    margin-right: 0;
    margin-left: 0;
    flex: none;
    width: 100%;
  }
`;

export const ProfileImage = styled.div`
  display: flex;
  flex: 2;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  width: 100%;
  height: 100%;
  background-image: url(${props => props.img});
`;

export const Details = styled.div`
  display: flex;
  flex: 4;
  padding: 12px 24px;
  flex-direction: column;
  overflow-y: auto;
  overflow-x: hidden;

  &::-webkit-scrollbar:vertical {
    width: 6px !important;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #355166cc;
    border-radius: 50px;
    border: 1px solid #234e6d;
  }
  &::-webkit-scrollbar-track {
    background-color: transparent;
    border-radius: 25px;
    border-radius: 50px;
  }
`;

export const Item = styled.span`
  display: block;
  font-size: 14px;
`;

export const UserOccupation = styled(Item)`
  color: rgba(255, 255, 255, 0.6);
  font-style: italic;
  font-size: 12px;
`;

export const Company = styled(Item)`
  margin: 4px 0;
  font-size: 16px;
`;

export const FancyLink = styled.a`
  transition: 0.2s;
  text-decoration: none;

  &:hover {
    color: #ffc600;
  }
`;

export const Group = styled.div`
  margin: 12px 0;
`;

export const About = styled(Item)`
  color: rgba(255, 255, 255, 0.6);
  font-size: 12px;
  line-height: 15px;
`;

export const Social = styled(Group)`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
`;

export const DisplayName = styled.h3`
  font-size: 22px;
  text-transform: uppercase;
  margin-top: 0;
  margin-bottom: 0;
`;

export const Emph = styled.span`
  font-weight: bold;
  padding-right: 4px;
  color: rgba(255, 255, 255, 0.6);
`;

export const SocialIcon = styled(FontAwesomeIcon)`
  font-size: 16px;
`;

export const LinkIcon = styled(FontAwesomeIcon)`
  font-size: 16px;
  padding-right: 6px;
`;

export const Position = styled.div`
  font-size: 14px;
`;
