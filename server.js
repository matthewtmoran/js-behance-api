const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const cors = require('cors');
const axios = require('axios');
const { API_KEY, USERS_URL, USER_URL } = require('./src/constants');

app.use(cors());
app.listen(port, () => console.log(`Listening on port ${port}`));

//get users with qeury param
app.get('/users', async (req, res) => {
  const query = `${USERS_URL}${req.query.term}/&api_key=${API_KEY}`;
  try {
    const response = await axios.get(query);
    res.send(response.data);
  } catch (err) {
    res.send(err);
    throw err;
  }
});

//get individual user data
app.get('/user/:id', async (req, res) => {
  const query = `${USER_URL}${req.params.id}/?api_key=${API_KEY}`;
  try {
    const response = await axios.get(query);
    res.send(response.data);
  } catch (err) {
    res.send(err);
    throw err;
  }
});

app.get('/user/:id/projects', async (req, res) => {
  const query = `${USER_URL}${req.params.id}/projects/?api_key=${API_KEY}`;
  try {
    const response = await axios.get(query);
    res.send(response.data);
  } catch (err) {
    res.send(err);
    throw err;
  }
});

app.get('/user/:id/work', async (req, res) => {
  const query = `${USER_URL}${
    req.params.id
  }/work_experience/?api_key=${API_KEY}`;
  try {
    const response = await axios.get(query);
    return res.send(response.data);
  } catch (err) {
    if (err.response.status === 404) {
      return res.send({ work_experience: [] });
    }
    return res.send(err);
  }
});

app.get('/user/:id/following', async (req, res) => {
  const query = `${USER_URL}${req.params.id}/following/?api_key=${API_KEY}`;
  try {
    const response = await axios.get(query);
    res.send(response.data);
  } catch (err) {
    res.send(err);
    throw err;
  }
});

app.get('/user/:id/followers', async (req, res) => {
  const query = `${USER_URL}${req.params.id}/followers/?api_key=${API_KEY}`;
  try {
    const response = await axios.get(query);
    res.send(response.data);
  } catch (err) {
    res.send(err);
    throw err;
  }
});
